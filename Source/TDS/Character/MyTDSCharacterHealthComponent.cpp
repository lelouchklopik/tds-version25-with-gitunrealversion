// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDSCharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"

void UMyTDSCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{

	float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UMyTDSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UMyTDSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UMyTDSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime,false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UMyTDSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UMyTDSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UMyTDSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}
